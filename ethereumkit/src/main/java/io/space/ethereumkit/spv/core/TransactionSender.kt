package io.space.ethereumkit.spv.core

import io.space.ethereumkit.core.ISpvStorage
import io.space.ethereumkit.core.TransactionBuilder
import io.space.ethereumkit.core.TransactionSigner
import io.space.ethereumkit.models.EthereumTransaction
import io.space.ethereumkit.spv.models.RawTransaction
import io.space.ethereumkit.spv.net.handlers.SendTransactionTaskHandler
import io.space.ethereumkit.spv.net.tasks.SendTransactionTask

class TransactionSender(private val storage: ISpvStorage,
                        private val transactionBuilder: TransactionBuilder,
                        private val transactionSigner: TransactionSigner) : SendTransactionTaskHandler.Listener {

    interface Listener {
        fun onSendSuccess(sendId: Int, transaction: EthereumTransaction)
        fun onSendFailure(sendId: Int, error: Throwable)
    }

    var listener: Listener? = null

    fun send(sendId: Int, taskPerformer: ITaskPerformer, rawTransaction: RawTransaction) {
        val accountState = storage.getAccountState() ?: throw NoAccountState()
        val signature = transactionSigner.signature(rawTransaction, accountState.nonce)

        taskPerformer.add(SendTransactionTask(sendId, rawTransaction, accountState.nonce, signature))
    }

    override fun onSendSuccess(task: SendTransactionTask) {
        val transaction = transactionBuilder.transaction(task.rawTransaction, task.nonce, task.signature)

        listener?.onSendSuccess(task.sendId, transaction)
    }

    override fun onSendFailure(task: SendTransactionTask, error: Throwable) {
        listener?.onSendFailure(task.sendId, error)
    }

    open class SendError : Exception()
    class NoAccountState : SendError()

}
